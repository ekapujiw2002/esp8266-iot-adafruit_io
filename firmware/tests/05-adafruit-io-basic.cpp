#include <Arduino.h>
#include <adafruit-config.h>

/*
 * define
 */
#define DEBUG_PORT Serial
#define LED LED_BUILTIN
#define SW_DOOR 13

/*
 * global variable
 */
volatile bool sw_state = false;

/*
 * adafruit io feeds
 */
AdafruitIO_Feed *digital_input = io.feed("digital-input"),
                *digital_output = io.feed("digital-output");

/*
 * proto function
 */
void handleMessage(AdafruitIO_Data *data);

/**
 * handle message receive from feeds
 * @method handleMessage
 * @param  data          [description]
 */
void handleMessage(AdafruitIO_Data *data) {
  // DEBUG_PORT.print("Receive data from ");
  // DEBUG_PORT.print(data->feedName());
  // DEBUG_PORT.print(" : ");
  // DEBUG_PORT.println(data->toString());
  DEBUG_PORT.printf_P(PSTR("[%20s] : %s\r\n"), data->feedName(),
                      (data->toString()).c_str());
}

/**
 * Setup all here
 * @method setup
 */
void setup() {
  DEBUG_PORT.begin(115200);
  pinMode(LED, OUTPUT);

  // switch input door
  pinMode(SW_DOOR, INPUT_PULLUP);

  // connect to io.adafruit.com
  DEBUG_PORT.print("Connecting to Adafruit IO");
  io.connect();

  // set up a message handler for the feed.
  // the handleMessage function (defined below)
  // will be called whenever a message is
  // received from adafruit io.
  digital_input->onMessage(handleMessage);
  digital_output->onMessage(handleMessage);

  // wait for a connection
  while (io.status() < AIO_CONNECTED) {
    DEBUG_PORT.print(".");
    delay(500);
    yield();
  }

  // we are connected
  Serial.println();
  Serial.println(io.statusText());
}

/**
 * The big loop
 * @method loop
 */
void loop() {
  // io.run(); is required for all sketches.
  // it should always be present at the top of your loop
  // function. it keeps the client connected to
  // io.adafruit.com, and processes any incoming data.
  io.run();

  // this is printed from your flash
  DEBUG_PORT.printf_P(PSTR("%10lu : HELLO WORLD %6lu\r\n"), millis(),
                      ESP.getFreeHeap());

  // read the switch using ordinary method
  if (!digitalRead(SW_DOOR)) {
    // some delay debouncing
    delay(100);

    digital_input->save((char *)"OPENED");

    // the smarter ways
    while (!digitalRead(SW_DOOR)) {
      delay(1);
      yield();
    }
    // sw_state = true;

    digital_input->save((char *)"CLOSED");
  }

  // if (sw_state) {
  //   sw_state = false;
  //   DEBUG_PORT.println(F("DOOR BUTTON PUSHED"));
  //   digital_input->save(sw_state);
  // }

  digitalWrite(LED, digitalRead(LED) ^ 1);

  delay(500);
}
