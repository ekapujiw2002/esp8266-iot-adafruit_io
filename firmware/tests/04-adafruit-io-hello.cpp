#include <Arduino.h>
#include <adafruit-config.h>

void setup() {
  // start the serial connection
  Serial.begin(115200);

  // wait for serial monitor to open
  // while (!Serial)
  //   ;

  // connect to io.adafruit.com
  Serial.println();
  Serial.print("Connecting to Adafruit IO");
  io.connect();

  // wait for a connection
  while (io.status() < AIO_CONNECTED) {
    Serial.print(".");
    delay(500);
    yield();
  }

  // we are connected
  Serial.println();
  Serial.println(io.statusText());
}

void loop() {

  // io.run(); is required for all sketches.
  // it should always be present at the top of your loop
  // function. it keeps the client connected to
  // io.adafruit.com, and processes any incoming data.
  io.run();
}
