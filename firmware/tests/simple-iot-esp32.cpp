/*
 * Added compatibility for ESP32
 */
#include <Arduino.h>
#include <common/adafruit-config.h>
#include <DHT.h>

#define PIN_LED LED_BUILTIN
#define DEBUG_PORT Serial

#define DHT22_SENSOR 26

DHT DHT_Sensor(DHT22_SENSOR, DHT22);
float DHT_Humidity, DHT_Temperature;

AdafruitIO_Feed *led = io.feed("led"), *suhu = io.feed("suhu"),
                *humx = io.feed("kelembapan");

void handleMessage(AdafruitIO_Data *data);

void handleMessage(AdafruitIO_Data *data) {
  DEBUG_PORT.printf_P(PSTR("[%20s] : %s\r\n"), data->feedName(),
                      (data->toString()).c_str());

  // proses datanya
  if (String(data->feedName()).equals("led")) {
    digitalWrite(PIN_LED, !(data->toString()).equals("ON"));
  }
}

void setup() {
  DHT_Sensor.begin();

  DEBUG_PORT.begin(9600);
  pinMode(PIN_LED, OUTPUT);

  led->onMessage(handleMessage);
  suhu->onMessage(handleMessage);
  humx->onMessage(handleMessage);

  DEBUG_PORT.println("CONNECTING TO ADAFRUIT...");
  io.connect();

  digitalWrite(PIN_LED, HIGH);

  // wait for a connection
  while (io.status() < AIO_CONNECTED) {
    DEBUG_PORT.print(".");
    delay(500);
    yield();
  }

  digitalWrite(PIN_LED, LOW);
  DEBUG_PORT.println(io.statusText());
}

void loop() {
  io.run();

  DHT_Humidity = DHT_Sensor.readHumidity();
  DHT_Temperature = DHT_Sensor.readTemperature();
  DEBUG_PORT.println(DHT_Temperature);
  DEBUG_PORT.println(DHT_Humidity);
  suhu->save(DHT_Temperature);
  delay(1000);
  humx->save(DHT_Humidity);

  delay(5000);
  digitalWrite(PIN_LED, !digitalRead(PIN_LED));
}
