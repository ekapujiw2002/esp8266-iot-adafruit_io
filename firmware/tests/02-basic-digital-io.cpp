/*
 * Hello world ESP8266
 */
#include <Arduino.h>
// #include <pgmspace.h>

/*
 * Some define
 */
#define DEBUG_PORT Serial
#define LED LED_BUILTIN
#define SW_DOOR 13
#define SSR_OUTPUT 5
#define FLAME_SENSOR 12
#define RCWL_SENSOR 14

/*
 * global variables
 */
volatile bool sw_state = false;
volatile bool flame_state = false;
volatile bool rcwl_state = false;

/*
 * proto void
 */
void button_handler();
void flame_sensor_handler();
void rcwl_sensor_handler();

/**
 * button handler using interrupt
 * @method button_handler
 */
void button_handler() { sw_state = true; }

void flame_sensor_handler() { flame_state = true; }

void rcwl_sensor_handler() { rcwl_state = true; }

/**
 * Setup all here
 * @method setup
 */
void setup() {
  DEBUG_PORT.begin(115200);
  pinMode(LED, OUTPUT);

  // switch input door
  pinMode(SW_DOOR, INPUT_PULLUP);

  // using interrupt
  // attachInterrupt(digitalPinToInterrupt(SW_DOOR), button_handler, FALLING);

  // ssr pin as output
  pinMode(SSR_OUTPUT, OUTPUT);
  digitalWrite(SSR_OUTPUT, LOW);

  // flame and rcwl sensor as input
  pinMode(FLAME_SENSOR, INPUT_PULLUP);
  pinMode(RCWL_SENSOR, INPUT_PULLUP);

  // set the interrupt
  attachInterrupt(digitalPinToInterrupt(FLAME_SENSOR), flame_sensor_handler,
                  RISING);
  attachInterrupt(digitalPinToInterrupt(RCWL_SENSOR), rcwl_sensor_handler,
                  RISING);
}

/**
 * The big loop
 * @method loop
 */
void loop() {
  // this is printed from your flash
  // DEBUG_PORT.println(F("HELLO WORLD ESP8266"));

  // this is printed from your flash also
  // DEBUG_PORT.printf(F("%10lu\t: HELLO WORLD\r\n"), millis());

  // this is printed from your ram
  DEBUG_PORT.printf("%10lu : HELLO WORLD %6lu\r\n", millis(),
                    ESP.getFreeHeap());

  // this is printed from your flash
  // DEBUG_PORT.printf_P(PSTR("%10lu : HELLO WORLD %6lu\r\n"), millis(),
  // ESP.getFreeHeap());

  // read the switch using ordinary method
  if (!digitalRead(SW_DOOR)) {
    // some delay debouncing
    delay(100);

    // do this and see what happened ;)
    while (!digitalRead(SW_DOOR))
      ;

    // // the smarter ways
    // while (!digitalRead(SW_DOOR)) {
    //   delay(10);
    //   yield();
    // }
    sw_state = true;
  }

  if (sw_state) {
    sw_state = false;
    DEBUG_PORT.println(F("DOOR BUTTON PUSHED"));
  }

  digitalWrite(LED, digitalRead(LED) ^ 1);

  delay(500);
}

// with printf_P
// text       data     bss     dec     hex filename
// 256274     3188   30096  289558   46b16 .pioenvs\nodemcuv2\firmware.elf
//
// with prinft
// text       data     bss     dec     hex filename
// 255970     3220   30096  289286   46a06 .pioenvs\nodemcuv2\firmware.elf
