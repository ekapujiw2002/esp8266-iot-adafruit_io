/*
 * Hello world ESP8266
 */
#include <Arduino.h>

/*
 * Some define
 */
#define DEBUG_PORT Serial

/**
 * Setup all here
 * @method setup
 */
void setup() { DEBUG_PORT.begin(115200); }

/**
 * The big loop
 * @method loop
 */
void loop() {
  // DEBUG_PORT.println(F("HELLO WORLD ESP8266"));
  // DEBUG_PORT.printf(F("%10lu\t: HELLO WORLD\r\n"), millis());
  DEBUG_PORT.printf("%10lu : HELLO WORLD\r\n", millis());
  delay(500);
}
