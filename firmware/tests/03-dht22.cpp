#include <Arduino.h>
#include <DHT.h>

#define DHT22_SENSOR 4

DHT DHT_Sensor(DHT22_SENSOR, DHT22);
float DHT_Humidity, DHT_Temperature;

void setup() {
  Serial.begin(115200);
  DHT_Sensor.begin();
}

void loop() {
  DHT_Humidity = DHT_Sensor.readHumidity();
  DHT_Temperature = DHT_Sensor.readTemperature();

  Serial.print(DHT_Temperature, 1);
  Serial.print("\t");
  Serial.println(DHT_Humidity, 1);
  delay(5000);
}
