/*
 * This is main file for SWIT project
 * Target : ESP8266 , ESP32
 */
#include <Arduino.h>
#include <DHT.h>
#include <common/adafruit-config.h>

/*
 * define
 */
#define DEBUG_PORT Serial
#define LED LED_BUILTIN

#if defined(ESP8266)
#define SW_DOOR 13
#define SSR_OUTPUT 5
#define FLAME_SENSOR 12
#define RCWL_SENSOR 14
#define DHT22_SENSOR 4
#elif defined(ESP32)
#define SW_DOOR 13
#define SSR_OUTPUT 27
#define FLAME_SENSOR 12
#define RCWL_SENSOR 14
#define DHT22_SENSOR 26
#else
#error "No IO definition for your board"
#endif

/*
 * global variable
 */
volatile bool sw_door_opened = false, flame_on = false, human_detected = false;
DHT DHT_Sensor(DHT22_SENSOR, DHT22);
float DHT_Humidity, DHT_Temperature;
long time_send_data = 0, time_send_dht22_data = 0;

/*
 * adafruit io feeds
 */
AdafruitIO_Feed *io_door = io.feed("door"), *io_flame = io.feed("flame"),
                *io_human = io.feed("human"), *io_lamp = io.feed("lamp"),
                *io_temperature = io.feed("temperature"),
                *io_humidity = io.feed("humidity");

/*
 * proto function
 */
void handleIOMessage(AdafruitIO_Data *data);
void swDoorHandler();
void flameSensorHandler();
void humanSensorHandler();

/**
 * handle message receive from feeds
 * @method handleMessage
 * @param  data          [description]
 */
void handleIOMessage(AdafruitIO_Data *data) {
  String feed_name = String(data->feedName());
  String feed_data = data->toString();

  DEBUG_PORT.printf_P(PSTR("[%20s] : %s\r\n"), feed_name.c_str(),
                      feed_data.c_str());

  // lamp on off
  if (feed_name.equals("lamp")) {
    digitalWrite(SSR_OUTPUT, feed_data.equals("ON"));
    DEBUG_PORT.printf_P(PSTR("LAMP = %d\r\n"), digitalRead(SSR_OUTPUT));
  }
}

/**
 * handler for door switch
 * @method swDoorHandler
 */
void swDoorHandler() { sw_door_opened = !digitalRead(SW_DOOR); }
void flameSensorHandler() { flame_on = digitalRead(FLAME_SENSOR); }
void humanSensorHandler() { human_detected = digitalRead(RCWL_SENSOR); }

/**
 * Setup all here
 * @method setup
 */
void setup() {
  // set the debug port
  DEBUG_PORT.begin(115200);

  // set the led pin
  pinMode(LED, OUTPUT);

  // switch input door
  pinMode(SW_DOOR, INPUT_PULLUP);

  // ssr pin as output
  pinMode(SSR_OUTPUT, OUTPUT);
  digitalWrite(SSR_OUTPUT, LOW);

  // flame and rcwl sensor as input
  pinMode(FLAME_SENSOR, INPUT_PULLUP);
  pinMode(RCWL_SENSOR, INPUT_PULLUP);

  // setup interrupt handler for all pin
  attachInterrupt(digitalPinToInterrupt(SW_DOOR), swDoorHandler, CHANGE);
  attachInterrupt(digitalPinToInterrupt(FLAME_SENSOR), flameSensorHandler,
                  RISING);
  attachInterrupt(digitalPinToInterrupt(RCWL_SENSOR), humanSensorHandler,
                  RISING);

  // init state
  sw_door_opened = !digitalRead(SW_DOOR);
  flame_on = digitalRead(FLAME_SENSOR);
  human_detected = digitalRead(RCWL_SENSOR);

  // init sensor DHT22
  DHT_Sensor.begin();

  // connect to io.adafruit.com
  DEBUG_PORT.print("\r\nConnecting to Adafruit IO");
  io.connect();

  // set up a message handler for the feed.
  // the handleMessage function (defined below)
  // will be called whenever a message is
  // received from adafruit io.
  io_door->onMessage(handleIOMessage);
  io_flame->onMessage(handleIOMessage);
  io_human->onMessage(handleIOMessage);
  io_humidity->onMessage(handleIOMessage);
  io_lamp->onMessage(handleIOMessage);
  io_temperature->onMessage(handleIOMessage);

  // wait for a connection
  while (io.status() < AIO_CONNECTED) {
    DEBUG_PORT.print(".");
    delay(500);
    yield();
  }

  // we are connected
  DEBUG_PORT.println();
  DEBUG_PORT.println(io.statusText());

  // init the time
  time_send_data = time_send_dht22_data = millis();
}

/**
 * The big loop
 * @method loop
 */
void loop() {
  // io.run(); is required for all sketches.
  // it should always be present at the top of your loop
  // function. it keeps the client connected to
  // io.adafruit.com, and processes any incoming data.
  io.run();

  // this is printed from your flash
  DEBUG_PORT.printf_P(PSTR("%10lu : %6lu\r\n"), millis(), ESP.getFreeHeap());

  // just some alive beat
  digitalWrite(LED, digitalRead(LED) ^ 1);

  // send humidity and temperature per 5sec
  if (millis() - time_send_dht22_data > 10000) {
    time_send_dht22_data = millis();

    // DEBUG_PORT.println(F("SEND DHT22 DATA"));

    // get dht data
    DHT_Humidity = DHT_Sensor.readHumidity();
    DHT_Temperature = DHT_Sensor.readTemperature();

    // send all data to adafruit io
    // io_temperature->save(DHT_Temperature);
    // io_humidity->save(DHT_Humidity);
    bool resx = io_temperature->save(DHT_Temperature);
    DEBUG_PORT.printf_P(PSTR("SEND TEMPERATURE ... %s\r\n"),
                        resx ? "OK" : "FAIL");

    delay(1000);
    resx = io_humidity->save(DHT_Humidity);
    DEBUG_PORT.printf_P(PSTR("SEND HUMIDITY ... %s\r\n"), resx ? "OK" : "FAIL");
  }

  // send all other data per 3sec
  if (millis() - time_send_data > 6000) {
    time_send_data = millis();

    // DEBUG_PORT.println(F("SEND OTHER DATA"));

    // io_door->save(sw_door_opened);
    // io_flame->save(flame_on);
    // io_human->save(human_detected);

    bool resx =
        io_door->save(sw_door_opened ? (char *)"OPENED" : (char *)"CLOSED");
    DEBUG_PORT.printf_P(PSTR("SEND DOOR STATUS ... %s\r\n"),
                        resx ? "OK" : "FAIL");

    delay(1000);
    resx = io_flame->save(flame_on ? (char *)"FIRE" : (char *)"-");
    DEBUG_PORT.printf_P(PSTR("SEND FIRE STATUS ... %s\r\n"),
                        resx ? "OK" : "FAIL");

    delay(1000);
    resx = io_human->save(human_detected ? (char *)"YES" : (char *)"NO");
    DEBUG_PORT.printf_P(PSTR("SEND HUMAN STATUS ... %s\r\n"),
                        resx ? "OK" : "FAIL");
  }

  delay(500);
}
