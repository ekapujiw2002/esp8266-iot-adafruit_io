SWIT
Simple and Awesome Home IoT System
===

![bg](media/global-hand.jpg)

---
<!-- page_number: true -->
<!-- prerender: true -->
<!-- $size: A4 -->

# What?

- **S.W.I.T**
  ```
  Just another simple but awesome IoT system that you can build
  on your own
  ```
- **Goal**
  ```
  Build IoT system that can give you information about : 
  temperature, humidity, human presence, and home appliance, 
  with 2 ways interaction
  ```
- **Technology Standard**
  ```
  WiFi, ARM, HTTP, MQTT, AJAX, WebSocket, IoT
  ```
- **Architecture**
  ```
  See next page...
  ```
  
---

# Architecture

![150%](media/blok-diagram.png)

---

# Ingredients
- **Hardware**
  - ESP8266 NodeMCU v1.0 or advance or ESP32 Dev Kit
  - Sensors (DHT22, Human Motion RCWL-0516, Fire Detection)    
  - Actuator (SSR)
- **Software**
  - Atom, Arduino, Platformio, Python, NodeJS
- **Connection**
  - WiFi, AdafruitIO, IFTTT
- ***Where to get it?***
  - Read on please.....

---

# ESP8266 NodeMCU
<center>
<img src="media/nodemcu-v1.0.png" width="100%"/>
</center>

---

# ESP8266 NodeMCU Pinout

<center>
<img src="media/nodemcu-pins.png"/>
</center>

---

# ESP32 DEV KIT
<center>
<img src="media/esp32.jpg" width="70%"/>
</center>

---

# ESP32 DEV KIT Pinout

<center>
<img src="media/esp32_doit_pinout.png"/>
</center>

---

# ESP32 Another Pinout

<center>
<img src="media/esp32_pinout.jpg"/>
</center>

---

# Sensors
<center>
<img src="media/sensors.png" width="100%"/>
</center>

---

# Where to get it?
- https://www.tokopedia.com/rctrader/new-version-nodemcu-lua-iot-wifi-cp2102-esp8266
- https://www.tokopedia.com/lek-electronics/rcwl-0516-microwave-radar-human-body-sensor
- https://www.tokopedia.com/rajacell/ir-infrared-flame-detection-sensor-module-pendeteksi-api-for-arduino
- https://www.tokopedia.com/rajacell/ssr-solid-state-relay-module-1-channel-5v-dc-high-level-for-arduino
- https://www.tokopedia.com/rajacell/dht22-sensor-temperature-humidity-sensor-suhu-kelembapan-arduino
- https://www.tokopedia.com/cncstorebandung/cnc-esp32-esp-32-doit-wifi-bluetooth-iot-esp-32s-development-board?src=topads

---

# Platformio
![bg original 50%](media/platformio.png)
- **What is it**
  - PlatformIO is an open source ecosystem for IoT development
  - Cross-platform IDE and unified debugger. 
  - Remote unit testing and firmware updates
  (http://platformio.org/)
- **Specification**
  - **Cross-platform** build system without external dependencies to the OS software: 400+ embedded boards, 15+ development platforms, 10+ frameworks
  - C/C++ Intelligent Code Completion and Smart Code Linter for rapid professional development
  - Multi-projects workflow with multiple panes and Themes support with dark and light colors
  - Built-in Terminal with PlatformIO Core and powerful Serial Port Monitor

---

# Platformio
- **System Requirements**
  - OS : Windows, macOS, Linux, FreeBSD, Linux ARMv6+
  - Python Interpreter : Python 2.7 only
  - CLang Interpreter : 3.9.1 version
- **WARNING**
  **Shut off all your virus and network protection before installing**

---

![bg original 100%](media/platformio-arch.png)

---

![bg original 80%](media/platformio-atom.png)

---

# Platformio
- **Configuration**
1. Install Python 2.7
   https://www.python.org/downloads/
2. Install CLang 3.9.1 (**Restart your PC after this installed**)
   http://releases.llvm.org/download.html
3. Install Atom Editor
   https://github.com/atom/atom/releases/tag/v1.23.3
4. Install Platformio
   `Menu: File > Settings > Install`
   <img src="media/ide-atom-pkg-installer.png" height="250" width="700"/>

---

# Driver CP2102

1. Download and install from :

https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers
	
---

![bg original 75%](media/python-windows-warning.png)

---

![bg original 75%](media/clang-installer-add-path.png)

---

# Platformio Quick Start

1. Run Atom, Platformio will be loaded

![80%](media/ide-atom-welcome.png)

---

# Platformio Quick Start

2. Click **New Project**, select the board, create it

![60%](media/ide-atom-new-project.png)

3. Please wait until it is finish installing all of the software it needed. **IT IS LONG FOR THE FIRST TIME**. Time for some coffee ^_^

---

# Platformio Quick Start

4. Create your source code at **src** folder, e.g. : *main.cpp*

  ```
  /**
   * Blink
   *
   * Turns on an LED on for one second,
   * then off for one second, repeatedly.
   */
  #include <Arduino.h>

  // Set LED_BUILTIN if it is not defined by Arduino framework
  // #define LED_BUILTIN 13

  void setup()
  {
    // initialize LED digital pin as an output.
    pinMode(LED_BUILTIN, OUTPUT);
  }
  ```

---

  ```
  void loop()
  {
    // turn the LED on (HIGH is the voltage level)
    digitalWrite(LED_BUILTIN, HIGH);

    // wait for a second
    delay(1000);

    // turn the LED off by making the voltage LOW
    digitalWrite(LED_BUILTIN, LOW);

     // wait for a second
    delay(1000);
  }
  ```

---

![bg original 70%](media/ide-atom-blink-project.png)

---

# Platformio Quick Start

5. Compile and upload your code to your board using **Build**, **Upload** menu
![85%](media/ide-atom-process-project.png)

---

# Platformio Quick Start

![90%](media/ide-atom-build-project.png)

---

# Platformio Quick Start

6. To run other target, press **F7** key
![90%](media/ide-atom-quick-start-8.png)

---

# Platformio Quick Start

7. Select the target you want to run, if any
![90%](media/ide-atom-quick-start-9.png)

---

# Platformio Quick Start

8. For advance usage, use the terminal
![95%](media/ide-atom-quick-start-10.png)

---

# Platformio Quick Start

![95%](media/ide-atom-quick-start-11.png)

---

# Platformio Quick Start

9. Debug your program using **Serial Port Monitor**

![95%](media/ide-atom-quick-start-12.png)

---

# Platformio Quick Start


![95%](media/ide-atom-quick-start-13.png)

---

# Platformio Quick Start


![95%](media/ide-atom-quick-start-14.png)

---

# Platformio Core

1. Open Atom Platformio
2. Open up the **Terminal** using **CTRL\+\`**
3. Get the board definition : `pio boards nodemcu` or `pio boards esp32doit`
4. Initialize the project : `pio init --board uno --board nodemcuv1.0 --board esp32doit-devkit-v1`
5. Configure **platformio.ini** file fo further customization

---

# Platformio Core

platformio.ini
```
; PlatformIO Project Configuration File
;
;   Build options: build flags, source filter
;   Upload options: custom upload port, speed and extra flags
;   Library options: dependencies, extra library storages
;   Advanced options: extra scripting
;
; Please visit documentation for the other options and examples
; http://docs.platformio.org/page/projectconf.html
[platformio]
env_default = nodemcuv2

; if you want add aonther library folder
; lib_extra_dirs = K:\2009\WORK\PROJECT\PlatformIO_Library\

[common]
lib_ldf_mode = deep+

[env:uno]
platform = atmelavr
```

---

```
board = uno
framework = arduino

[env:nodemcuv2]
platform = espressif8266
board = nodemcuv2
framework = arduino
; 4M (1M SPIFFS)
build_flags = -Wl,-Tesp8266.flash.4m1m.ld

; 4M (3M SPIFFS)
; build_flags = -Wl,-Tesp8266.flash.4m.ld -lc

; make sure the library search it deep enough
; lib_ldf_mode = deep+

; customize upload port
upload_port = COM4
upload_speed = 115200

; src_filter = -<test/>
; src_filter = +<*> -<arduino> -<test>
```
---

```
; you environment, name it as you wish, no space
[env:esp32dev]
; platform used
platform = espressif32
; platform = espressif32@0.11.1
; platform = https://github.com/platformio/
;            platform-espressif32.git

; framework use for this environment
; depend on your board and platform
framework = arduino

; Board name used
board = esp32doit-devkit-v1

; http://docs.platformio.org/en/latest/projectconf/
; section_env_library.html
; how the platformio search for Library
; deep+ is the most advance and 
; complete for auto search Library
lib_ldf_mode = deep+
```
---

```
; which library you want to ignore or exclude from compilation
; lib_ignore = ESPAsync

; which library you want to use specifically for this environment
; lib_deps = DHT

; how the library is automagically choose, 
; which one is going to be compiled, 
; which one is not
; by default soft mode, 
; platformio will ignore library which is 
; not meant for the framework
; lib_compat_mode = soft

; if you got many folder inside src, then this will filter
; which one you want to include(+) or exclude(-)
; src_filter = +<*> -<release>

; add custom build flags
; -fexceptions needed if you want to use BLE
; build_flags = -fexceptions

; customize upload port, if needed
; upload_port = COM4
; upload_speed = 115200
```

---

# Platformio Tips

- Disable auto update -- **It is very irritating**
![](media/platformio-tips1.png)

---

# Platformio Tips

- Disable Telemetry -- **It is consuming your bandwidth**
![](media/platformio-tips2.png)

---

# Platformio Tips

- Show line number and wrap text -- **It is nice on the editor**
![](media/platformio-tips3.png)

---

# Platformio Tips

- Choose some soft theme -- **Good for your eyes**
![](media/platformio-tips4.png)

---

# Platformio Tips

- Add some add on packages -- **Make coding for productive**
	- **atom-beautify, dockblockr, minimap**
![50%](media/platformio-tips5.png)

---

# ESP8266 NodeMCU
- **What is it**
  
`An open-source firmware and development kit that helps you to prototype your IOT product within a few Lua script lines (https://github.com/nodemcu/nodemcu-devkit-v1.0)`

- **Specification**
	- ESP8266 12E processor
	- Flash 4MB
	- RAM 80KB
	- 80MHz - 160MHz clock frequency
	- WiFi station and AP support
	- 16 GPIO
	- I2C, USART, SPI

---

# ESP8266 NodeMCU

- **Programming**
	- Using Arduino Framework and Platformio
	- Board name : nodemcuv2
	- Repository : https://github.com/esp8266/Arduino
	- Documentation : https://arduino-esp8266.readthedocs.io/en/2.4.0/

---

# ESP32 Doit Dev Kit (clone of ESP32 Dev KitC)
- **What is it**
  
`ESP32-DevKit is an entry-level development board. It has all the ESP32 pins exposed and is easy to connect and use. Build with ESP32 chip from Espressif. Sucessor of ESP8266` https://www.espressif.com/en/products/hardware/esp32-devkitc/overview

- **Specification**
	- Dual-core system with two Harvard Architecture Xtensa LX6 CPUs
	- Flash 4MB
	- RAM 520KB
	- 160MHz - 240Mhz clock frequency
	- WiFi station and AP support + BLE
	- 20+ GPIO
	- I2C, USART, SPI, RTC, ETHERNET, I2S, 16Ch PWM, AES

---

# ESP32 Doit Dev Kit

- **Programming**
	- Using Arduino Framework and Platformio
	- Board name : esp32doit-devkit-v1
	- Repository : https://github.com/espressif/arduino-esp32
	- Documentation : -
	
---

# ESP8266 NodeMCU - ESP32
- **Hello world**

  ```
  /**
   * Blink
   *
   * Turns on an LED on for one second,
   * then off for one second, repeatedly.
   */
  #include <Arduino.h>

  // Set LED_BUILTIN if it is not defined by Arduino framework
  // #define LED_BUILTIN D0

  void setup()
  {
    // initialize LED digital pin as an output.
    pinMode(LED_BUILTIN, OUTPUT);
  }
  ```

---

  ```
  void loop()
  {
    // turn the LED on (HIGH is the voltage level)
    digitalWrite(LED_BUILTIN, HIGH);

    // wait for a second
    delay(1000);

    // turn the LED off by making the voltage LOW
    digitalWrite(LED_BUILTIN, LOW);

     // wait for a second
    delay(1000);
  }
  ```
  
---

# ESP8266 NodeMCU - ESP32 Arduino Style

- Basic Digital Input Output
1. Configure GPIO `pinMode(pin,mode)`
   **pin** : pin number
   **mode** : INPUT, OUTPUT, INPUT_PULLUP, INPUT_PULLDOWN_16 (GPIO16 Only)
   
2. Write to GPIO `digitalWrite(pin,value)`
   **pin** : pin number
   **value** : HIGH, LOW
   
3. Read GPIO `digitalRead(pin)`
   **pin** : pin number
   **return** : HIGH, LOW
   
---

# ESP8266 NodeMCU - ESP32 Arduino Style

- Basic Analog Input Output 
1. Read Analog Input 0-3V : `analogread(A0)`
2. Analog Output (PWM) 1Khz :
   a. Set analog output 0-1023 : `analogWrite(pin, value)`
   b. Change frequency : `analogWriteFreq(frequency)`
   
---

# ESP8266 NodeMCU - ESP32 Arduino Style

- Timing and Delay
    a. Timing in ms and us : `millis(), micros()` 	
    b. Delay : `delay(ms), delayMicroseconds(us)`
    c. Yield : `yield()`
    
---

# ESP8266 NodeMCU - ESP32 Arduino Style

- Serial Port
	- Nifty tool to debug your program
	- Object : `Serial, Serial1`
	- Init : `Serial.begin(speed)` 
	- Enable debug output : `Serial.setDebugOutput(true)`
	- Write data : `Serial.print(data), println, printf`
	- Read data : `Serial.read()`

---

# DHT22
- Specification
  - Low cost
  - 3 to 5V power and I/O
  - 2.5mA max current use during conversion (while requesting data)
  - Good for 0-100% humidity readings with 2-5% accuracy
  - Good for -40 to 80°C temperature readings ±0.5°C accuracy
  - No more than 0.5 Hz sampling rate **(once every 2 seconds)**
  - Body size 27mm x 59mm x 13.5mm (1.05" x 2.32" x 0.53")
  - 4 pins, 0.1" spacing
  - Weight (just the DHT22): 2.4g

---

# DHT22

- Wiring and Programming
- Library : https://github.com/adafruit/DHT-sensor-library
- Wiring :

![80%](media/dht22-conn.png) 

---

# DHT22 Sample Code

  ```
  // Example testing sketch for various DHT 
  // humidity/temperature sensors
  // Written by ladyada, public domain

  #include "DHT.h"

  #define DHTPIN 2     // what digital pin we're connected to

  // Uncomment whatever type you're using!
  //#define DHTTYPE DHT11   // DHT 11
  #define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
  //#define DHTTYPE DHT21   // DHT 21 (AM2301)

  // Connect pin 1 (on the left) of the sensor to +5V
  // NOTE: If using a board with 3.3V logic like an 
  // Arduino Due connect pin 1
  // to 3.3V instead of 5V!
  // Connect pin 2 of the sensor to whatever your DHTPIN is
  // Connect pin 4 (on the right) of the sensor to GROUND
  // Connect a 10K resistor from pin 2 (data) to pin 1 (power) 
  // of the sensor
  ```

---

# DHT22 Sample Code

  ```
  // Initialize DHT sensor.
  // Note that older versions of this library took 
  // an optional third parameter to
  // tweak the timings for faster processors.  
  // This parameter is no longer needed
  // as the current DHT reading algorithm adjusts 
  // itself to work on faster procs.
  DHT dht(DHTPIN, DHTTYPE);

  void setup() {
    Serial.begin(9600);
    Serial.println("DHTxx test!");

    dht.begin();
  }
  ```

---

# DHT22 Sample Code

  ```
  void loop() {
    // Wait a few seconds between measurements.
    delay(2000);

    // Reading temperature or humidity takes about 250 milliseconds!
    // Sensor readings may also be up to 2 seconds 'old' 
    // (its a very slow sensor)
    float h = dht.readHumidity();
    // Read temperature as Celsius (the default)
    float t = dht.readTemperature();
    // Read temperature as Fahrenheit (isFahrenheit = true)
    float f = dht.readTemperature(true);

    // Check if any reads failed and exit early (to try again).
    if (isnan(h) || isnan(t) || isnan(f)) {
      Serial.println("Failed to read from DHT sensor!");
      return;
    }
  ```

---

# DHT22 Sample Code

  ```
    // Compute heat index in Fahrenheit (the default)
    float hif = dht.computeHeatIndex(f, h);
    // Compute heat index in Celsius (isFahreheit = false)
    float hic = dht.computeHeatIndex(t, h, false);

    Serial.print("Humidity: ");
    Serial.print(h);
    Serial.print(" %\t");
    Serial.print("Temperature: ");
    Serial.print(t);
    Serial.print(" *C ");
    Serial.print(f);
    Serial.print(" *F\t");
    Serial.print("Heat index: ");
    Serial.print(hic);
    Serial.print(" *C ");
    Serial.print(hif);
    Serial.println(" *F");
  }
  ```
  
---

# RCWL-0516

- Specification :
  - Supply Voltage: 5V - 24V DC
  - Range max 7m
  - Can penetrate wall
  - Supply Current: >3mA (2.8mA typical)
  - Operating frequency: ~3.2GHz
  - Transmit power: 20mW (typical) / 30mW (max)
  - Size: 36mm x 17mm

---

# RCWL-0516

- Notes :
  - Component side should face detection area for maximum sensitivity 
  - Do not place metal objects within 1cm of antenna
  - Trigger out - HIGH (3.3V) motion / LOW no motion
  - 3.3V output is for reference only - do not use to power other devices.
  - Device operates at ~3.2GHz/20 - 30mW. In some countries it may be illegal to operate this device.

---

# RCWL-0516

- Pinout

|Pin|Name|Note|
|---|---|---|
|1|3V3|Regulated output (reference only)
|2|GND|Ground / 0V supply input
|3|OUT|Trigger: HIGH (3.3V) motion detected / LOW (0V) idle
|4|VIN|5V-24V supply input
|5|CDS|Sensor disable input (pull low to disable)

---

# RCWL-0516

- Wiring and Programming : Just use `digitalRead`

![130%](media/rcwl-0516-wiring.png)

---

# IR Fire Detector

- Specification
  - Detects a flame or a light source of a wavelength in the range of 760nm-1100 nm
  - Detection distance: 20cm (4.8V) ~ 100cm (1V)
  - Detection angle about 60 degrees, it is sensitive to the flame spectrum.
  - Comparator chip LM393 makes module readings stable.
  - Adjustable detection range.
  - Operating voltage 3.3V-5V
  - Digital and Analog Output DO digital switch outputs (0 and 1) AO analog voltage output

---

# IR Fire Detector

- Pinout

|Pin|Name|Note|
|---|---|---|
|1|VCC|3.3 - 5V
|2|GND|GND
|3|DO|Digital output (High when detect flame)
|4|AO|Analog voltage output

- Wiring and Programming :
  - Just connect it to GPIO and read using `digitalRead()`

---

# SSR (Solid State Relay)

- Just simple and safe AC load switch (so you wont get shocked LOL)
- Specification (OMRON G3MB-202P):
  - Manufacturer: OMRON
  - Part number: G3MB-202P, 5V version.
  - Isolation: Phototriac.
  - Zero cross: Yes.
  - Rated output load: 2A at 100 to 240V AC.(50/60Hz).

---

# SSR (Solid State Relay)

- Input control signal voltage:

|Voltage|Status|
|---|---|
|0V – 0.5V|Low stage (SSR is OFF)
|0.5V – 2.5V|(unknown state)
|2.5V – 20V|High state (SSR is ON)

- SSR Output (each channel):
  - Load voltage range: 75 to 264V AC (50/60Hz).
  - Load current: 0.1A to 2A

- Power supply: 5VDC / 160mA all 8x channel ON. (20mA each)

---

# SSR (Solid State Relay)

- Input section
  - DC +: positive power supply (relay voltage power supply)
  - DC-: connect power negative
  - CH1: 1 signal to trigger the end of the relay module (low level trigger valid)

- Output Section
  - A1: Relay Normally Open point A
  - B1: Relay Common

---

# SSR (Solid State Relay)

![150%](media/ssr1.jpg) ![150%](media/ssr2.jpg)

---

# Adafruit IO - IoT for Everyone
![bg original 70%](media/adafruit-io-logo.png)
- **What is it?**
> Adafruit IO is a system that makes data useful. Our focus is on ease of use, and allowing simple data connections with little programming required
- **Feature**
>1. Powerful API
>2. Beautiful Dashboards
>3. Private & Secure
>4. Full fledge documentation for many hardware and platforms
>5. Two way interactions

---

# Adafruit IO - IoT for Everyone

- Signup

![170%](media/adafruit-io-package.png)

---

# Adafruit IO - IoT for Everyone

- Feed and Dashboard
  - Feeds are the core of the Adafruit IO 
  - Holds metadata about the data you push (public or private, license the stored sensor data falls under, and a general description of the data)
  - Also contains the sensor data values that get pushed to Adafruit IO from your device.

---

# Adafruit IO - IoT for Everyone
https://learn.adafruit.com/adafruit-io-basics-feeds?view=all
- Create Feed

![110%](media/adafruit-io-feed-create.png)

---

# Adafruit IO - IoT for Everyone

- Create Feed

![120%](media/adafruit-io-feed-create-01.png)

---

# Adafruit IO - IoT for Everyone

- Create Feed

![100%](media/adafruit-io-feed-create-02.png)

---

# Adafruit IO - IoT for Everyone

- Create Feed

![100%](media/adafruit-io-feed-create-03.png)

---

# Adafruit IO - IoT for Everyone

- Create Feed

![120%](media/adafruit-io-feed-create-04.png)

---

# Adafruit IO - IoT for Everyone

- Create Feed

![90%](media/adafruit-io-feed-create-05.png)

---

# Adafruit IO - IoT for Everyone

- Dashboard

![](media/adafruit-io-dashboard-01.png)

---

# Adafruit IO - IoT for Everyone

- Dashboard

![](media/adafruit-io-dashboard-02.png)

---

# Adafruit IO - IoT for Everyone

- Dashboard

![](media/adafruit-io-dashboard-03.png)

---

# Adafruit IO - IoT for Everyone

- Dashboard

![](media/adafruit-io-dashboard-04.png)

---

# Adafruit IO - IoT for Everyone

- Dashboard

![](media/adafruit-io-dashboard-05.png)

---

# Adafruit IO - IoT for Everyone

- Dashboard

![](media/adafruit-io-dashboard-06.png)

---

# Adafruit IO - IoT for Everyone

- Dashboard Blocks

![](media/adafruit-io-dashboard-07.png)

---

# Adafruit IO - IoT for Everyone

- Dashboard Blocks

![80%](media/adafruit-io-dashboard-08.png)

---

# Adafruit IO - IoT for Everyone

- Feeds to Blocks

![](media/adafruit-io-dashboard-09.png)

---

# Adafruit IO - IoT for Everyone

- Feeds to Blocks

![](media/adafruit-io-dashboard-10.png)

---

# Adafruit IO - IoT for Everyone

- Feeds to Blocks

![120%](media/adafruit-io-dashboard-11.png)

---

# Adafruit IO - IoT for Everyone

- API Library

|Platform|URL|
|---|---|
|Arduino|https://github.com/adafruit/Adafruit_MQTT_Library
|Ruby|https://github.com/adafruit/io-client-ruby
|Python|https://github.com/adafruit/io-client-python
|Node.js|https://github.com/adafruit/io-client-node

---

# IFTTT
- **What is it**

> Free platform that helps you do more with all your apps and devices based on **Applets** and **Services**
> **Applets** : bring your services together to create new experiences
> **Services** : apps and devices you use everyday

- Feature

> 1. Syndicate data via social channels
> 2. Create corresponding triggers to actions
> 3. Connect online applications
> 4. 300+ application integrations
> 5. IOS, Android, Google Play, Twitter support
> 6. Allows auto-backups

---

# IFTTT
- **Signup**
  - Just go to : https://ifttt.com/join
- **Recipes**
  - Find it at : https://ifttt.com/discover
  - AdafruitIO : https://ifttt.com/adafruit

---

# SWIT Building
- **Hardware Wiring ESP8266**

![70%](media/swit-koneksi.png)

---

# SWIT Building
- **Hardware Wiring ESP32**

![66%](media/swit-esp32-koneksi.png)

---

# SWIT Building
- **Pin Allocation**

|Sensor|Pin|GPIO ESP8266|GPIO ESP32|
|---|---|---|---|
|Push Button|1|13|13|
|Flame|DO|12|12|
|RCWL|OUT|14|14|
|SSR|CH1|5|27|
|DHT22|DATA|4|26|

> Please follow your instructor direction for assembly and cabling

---

# SWIT Building
- **Software Programming**
1. Signup for account at **AdafruitIO, GitLab, IFTTT**
2. Install **PlatformIO**
3. Make new project based on NodeMCU v1.0 ESP8266
4. Open up this GitLab repository for the detail. Before that, please inform your GitLab account to your instructor. **https://gitlab.com/ekapujiw2002/esp8266-iot-adafruit_io**

- **Testing**

---

# Finish
- Showoff

---

# ESP32 Reference

- https://github.com/espressif/arduino-esp32
- https://www.espressif.com/en/products/hardware/esp32/overview
- http://www.alselectro.com/esp-32-iot-development-board.html
- https://startingelectronics.org/articles/ESP32-WROOM-testing/
- https://randomnerdtutorials.com/esp32-bluetooth-low-energy-ble-arduino-ide/
- https://docs.zerynth.com/latest/official/board.zerynth.nodemcu_esp32/docs/index.html

---

- https://docs.zerynth.com/latest/official/board.zerynth.doit_esp32/docs/index.html
- https://idyl.io/doit-esp32-development-board-review/
- http://www.iotsharing.com/2017/08/esp32-tutorials.html
- https://openhomeautomation.net/getting-started-esp32
- https://www.dfrobot.com/blog-964.html
- https://randomnerdtutorials.com/getting-started-with-esp32/
- https://techtutorialsx.com/category/esp32/page/14/
- https://github.com/sparkfun/ESP32_Miscellany